// db.fruits.insertMany([

/* 1. ============================================*/

db.fruits.aggregate([

		{
			$match: {onSale: true}
		},
		{
			$group: {_id: "$supplier_id", fruitOnSale: { $count: {} }}
		},
		{
			$project: {_id: 0}
		}

	]);



/* 2. ============================================*/

db.fruits.aggregate([

		{
               $match: {stock: {$gt: 20}}
        },
		{
               $group: {_id: "$stock", fruitsStocks: { $count: {} }}
        },
        {
               $project: {_id: 0}
        }

	]);


/* 3. ============================================*/

db.fruits.aggregate([
		{ 
			$match: {onSale: true} 
		},
		{ 
			$group: {_id: "$supplier_id", averagePrice: {$avg: "$stock"}}
		}
	]);



/* 4. ============================================*/

db.fruits.aggregate([

		{ 
			$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}
		}

	]);


/* 5. ============================================*/

db.fruits.aggregate([

		{ 
			$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}
		}

	]);

// 		{
// 			"name": "Apple",
// 			"color": "Red",
// 			"stock": 20,
// 			"price": 40,
// 			"supplier_id": 1,
// 			"onSale": true,
// 			"origin": ["Philippines", "US", ]
// 		},
// 		{
// 			"name": "Banana",
// 			"color": "yellow",
// 			"stock": 15,
// 			"price": 20,
// 			"supplier_id": 2,
// 			"onSale": true,
// 			"origin": ["Philippines", "Equador", ]
// 		},
// 		{
// 			"name": "Kiwi",
// 			"color": "Green",
// 			"stock": 25,
// 			"price": 50,
// 			"supplier_id": 1,
// 			"onSale": true,
// 			"origin": ["Philippines", "US", ]
// 		},
// 		{
// 			"name": "Mango",
// 			"color": "Yello",
// 			"stock": 10,
// 			"price": 120,
// 			"supplier_id": 2,
// 			"onSale": false,
// 			"origin": ["Philippines", "India", ]
// 		}

// 	]);